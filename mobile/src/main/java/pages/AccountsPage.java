package pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

import java.util.List;

public class AccountsPage extends PageObject {

    @AndroidFindBy(xpath = "//*[contains(@resource-id, 'toolbar')]//*[@text='Accounts']")
    private WebElementFacade accountsHeaderTitle;

    @AndroidFindBy(id = "org.gnucash.android:id/primary_text")
    private List<WebElementFacade> listAvailableAccounts;

    @AndroidFindBy(id = "org.gnucash.android:id/fab_create_account")
    private WebElementFacade createAccountPlusIconBottomRightCorner;

    public WebElementFacade getAccountsHeaderTitle() {
        return accountsHeaderTitle;
    }

    public List<WebElementFacade> getListAvailableAccounts() {
        return listAvailableAccounts;
    }

    public WebElementFacade getCreateAccountPlusIconBottomRightCorner() {
        return createAccountPlusIconBottomRightCorner;
    }
}
