package pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;


public class CreateAccountPage extends PageObject {

    @AndroidFindBy(id = "org.gnucash.android:id/input_account_name")
    private WebElementFacade inputAccountNameTextArea;

    @AndroidFindBy(id = "org.gnucash.android:id/input_currency_spinner")
    private WebElementFacade inputCurrencySpinner;

    @AndroidFindBy(id = "org.gnucash.android:id/input_color_picker")
    private WebElementFacade inputColorPicker;

    @AndroidFindBy(id = "org.gnucash.android:id/input_account_description")
    private WebElementFacade inputAccountDescTextArea;

    @AndroidFindBy(id = "org.gnucash.android:id/menu_save")
    private WebElementFacade saveButtonTopRight;

    public WebElementFacade getInputAccountNameTextArea() {
        return inputAccountNameTextArea;
    }

    public WebElementFacade getInputCurrencySpinner() {
        return inputCurrencySpinner;
    }

    public WebElementFacade getInputColorPicker() {
        return inputColorPicker;
    }

    public WebElementFacade getInputAccountDescTextArea() {
        return inputAccountDescTextArea;
    }

    public WebElementFacade getSaveButtonTopRight() {
        return saveButtonTopRight;
    }
}
