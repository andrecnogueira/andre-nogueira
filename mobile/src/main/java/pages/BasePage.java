package pages;

import net.thucydides.core.pages.PageObject;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;
import net.thucydides.core.webdriver.WebDriverFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

import static java.time.temporal.ChronoUnit.SECONDS;

public class BasePage extends PageObject {

    private WebDriver driver = ((WebDriverFacade) ThucydidesWebDriverSupport.getDriver()).getProxiedDriver();
    private HashMap<String, Integer> timeoutValues = new HashMap<String, Integer>() {{
        put("SHORT",2);
        put("MEDIUM",10);
        put("LONG",15);
    }};

    public WebElement findElementByAttributeAndText(String attribute, String text) {
        return driver.findElement(By.xpath("//*[@"+ attribute +"='"+ text +"']"));
    }

    public void setImplicitTimeout(String duration) {
        setImplicitTimeout(timeoutValues.get(duration), SECONDS);
    }
}
