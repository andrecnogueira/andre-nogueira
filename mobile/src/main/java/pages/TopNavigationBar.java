package pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class TopNavigationBar extends PageObject {

    @AndroidFindBy(id = "org.gnucash.android:id/menu_search")
    private WebElementFacade menuSearchIcon;

    @AndroidFindBy(id = "org.gnucash.android:id/search_src_text")
    private WebElementFacade searchTextArea;

    public WebElementFacade getMenuSearchIcon() {
        return menuSearchIcon;
    }

    public WebElementFacade getSearchTextArea() {
        return searchTextArea;
    }
}
