package pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class UserOnboardingPage extends PageObject {

    @AndroidFindBy(id = "android:id/title")
    private WebElementFacade welcomeMessageTitle;

    @AndroidFindBy(id = "org.gnucash.android:id/btn_save")
    private WebElementFacade nextButton;

    public WebElementFacade getWelcomeMessageTitle() {
        return welcomeMessageTitle;
    }

    public WebElementFacade getNextButton() {
        return nextButton;
    }
}
