package steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.model.TestStep;
import pages.BasePage;
import pages.CreateAccountPage;

import java.util.HashMap;

public class CreateAccountSteps extends TestStep {

    private BasePage basePage;

    private CreateAccountPage createAccountPage;

    private HashMap<String, String> listOfColors = new HashMap<String, String>() {{
        put("Dark Blue","Color 3");
        put("Green","Color 10");
    }};

    @Step
    public void createNewAccount(HashMap<String, String> accountSpecs) {
        createAccountPage.getInputAccountNameTextArea().
                type(accountSpecs.get("accountName"));

        createAccountPage.getInputCurrencySpinner().click();
        basePage.findElementByAttributeAndText(
                "text", accountSpecs.get("currency")).click();

        createAccountPage.getInputColorPicker().click();
        basePage.findElementByAttributeAndText(
                "content-desc", listOfColors.get(accountSpecs.get("accountColor"))).click();

        createAccountPage.getInputAccountDescTextArea().
                type(accountSpecs.get("accountDesc"));
    }

    @Step
    public void clickSaveAccount() {
        createAccountPage.getSaveButtonTopRight().click();
    }

    @Step
    public void cancelAccountSetup() {
        basePage.findElementByAttributeAndText(
                "content-desc", "Navigate up").click();
    }
}
