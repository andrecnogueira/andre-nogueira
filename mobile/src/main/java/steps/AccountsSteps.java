package steps;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.model.TestStep;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.NoSuchElementException;
import pages.AccountsPage;
import pages.BasePage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class AccountsSteps extends TestStep {

    private BasePage basePage;
    private AccountsPage accountsPage;
    private SoftAssertions softAssertions;

    @Step
    public void dismissReleaseNotes() {
        basePage.findElementByAttributeAndText("text", "DISMISS").click();
    }

    @Step
    public void verifyUserIsOnAccountPage() {
        assertTrue(accountsPage.getAccountsHeaderTitle().isDisplayed());
    }

    @Step
    public void verifyFiltering(List<String> expectedItems) {
        List<String> availableItems = new ArrayList<>(Arrays.asList());

        for (WebElementFacade accountType : accountsPage.getListAvailableAccounts()) {
            availableItems.add(accountType.getText());
        }

        assertTrue(availableItems.containsAll(expectedItems)
                && expectedItems.containsAll(availableItems));
    }

    @Step
    public void clickCreateAccountButton() {
        accountsPage.getCreateAccountPlusIconBottomRightCorner().click();
    }

    @Step
    public void validateAccountHasBeenCreated(String accountName) {
        basePage.findElementByAttributeAndText("text", accountName).isDisplayed();
    }

    @Step
    public void verifyAccountDoesNotExist(String accountName) {
        //Sets timeout to SHORT to prevent WebDriver pooling for element for 10 secs
        basePage.setImplicitTimeout("SHORT");

        try {
            basePage.findElementByAttributeAndText("text", accountName)
                    .isDisplayed();
        } catch (NoSuchElementException e) {
            //catch block to reset timeout value to default one
            //and return exception to test method as expected
            basePage.resetImplicitTimeout();
            throw new NoSuchElementException("Element not found");
        }
    }
}
