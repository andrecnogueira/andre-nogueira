package steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.model.TestStep;
import pages.BasePage;
import pages.UserOnboardingPage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class OnboardingSteps extends TestStep {

    private UserOnboardingPage userOnboardingPage;
    private BasePage basePage;

    @Steps(shared = true)
    OnboardingSteps onboardingSteps;

    @Steps(shared = true)
    AccountsSteps accountsSteps;

    @Step
    public void clickNextButton() {
        userOnboardingPage.getNextButton().click();
    }

    @Step
    public void selectOnboardingOptions(List<String> onboardingOptions) {
        clickNextButton();
        for (String option : onboardingOptions) {
            basePage.findElementByAttributeAndText("text", option).click();
            clickNextButton();
        }
    }

    @Step
    public void reviewSetupAndConfirm(List<String> myOptions) {
        for (String option : myOptions) {
            assertTrue(basePage.findElementByAttributeAndText("text", option).isEnabled());
        }
        clickNextButton();
    }

    @Step
    public void doOnboarding() {
        //Performs onboarding for other Test Classes as starting the
        //app from scratch is a good practice.
        List<String> onboardingOptions = new ArrayList<>(Arrays.asList(
                "USD",
                "Create default accounts",
                "Automatically send crash reports"
        ));
        onboardingSteps.selectOnboardingOptions(onboardingOptions);
        onboardingSteps.reviewSetupAndConfirm(onboardingOptions);
        accountsSteps.dismissReleaseNotes();
        accountsSteps.verifyUserIsOnAccountPage();
    }
}
