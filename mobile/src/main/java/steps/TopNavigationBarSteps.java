package steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.model.TestStep;
import pages.TopNavigationBar;

public class TopNavigationBarSteps extends TestStep {

    private TopNavigationBar topNavigationBar;

    @Step
    public void clickSearchMenu() {
        topNavigationBar.getMenuSearchIcon().click();
    }

    @Step
    public void doSearch(String searchText) {
        topNavigationBar.getSearchTextArea().type(searchText);
    }
}
