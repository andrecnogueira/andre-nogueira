package features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import steps.AccountsSteps;
import steps.OnboardingSteps;
import steps.TopNavigationBarSteps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SerenityRunner.class)
public class VerifySearch {

    @Managed()
    WebDriver webDriver;

    @Steps
    OnboardingSteps onboardingSteps;

    @Steps
    TopNavigationBarSteps topNavBarSteps;

    @Steps
    AccountsSteps accountsSteps;

    @Before
    public void doOnboarding() {
        onboardingSteps.doOnboarding();
    }

    @Test
    @Title("When I search for an account, filtering shall apply")
    public void verifySearchAndFiltering() {
        List<String> expectedAccounts = new ArrayList<>(Arrays.asList(
                "Assets",
                "Current Assets"
        ));
        topNavBarSteps.clickSearchMenu();

        topNavBarSteps.doSearch("Assets");

        accountsSteps.verifyFiltering(expectedAccounts);
    }
}
