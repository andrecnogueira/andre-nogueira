package features.accounts;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.NoSuchElementException;
import pages.BasePage;
import steps.AccountsSteps;
import steps.CreateAccountSteps;
import steps.OnboardingSteps;

import java.util.HashMap;


@RunWith(SerenityRunner.class)
public class VerifyAccountsFlow {

    @Managed
    WebDriver webDriver;

    @Steps
    OnboardingSteps onboardingSteps;

    @Steps
    AccountsSteps accountsSteps;

    @Steps
    CreateAccountSteps createAccountSteps;

    private BasePage basePage;

    @Before
    public void doOnboarding() {
        onboardingSteps.doOnboarding();
    }

    @Test
    @Title("User should be able to create a custom account using different currency" +
            "rather than chosen one during onboarding")
    public void createCustomAccountUsingDifferenceCurrency() {
        //Given I have my new account details
        HashMap<String, String> accountInfo = new HashMap<String, String>() {{
            put("accountName","Holidays in Paraguay");
            put("accountColor", "Green");
            put("currency", "UYU - Peso Uruguayo");
            put("accountDesc", "My trip to South America");
        }};

        //When I create it
        accountsSteps.clickCreateAccountButton();
        createAccountSteps.createNewAccount(accountInfo);
        createAccountSteps.clickSaveAccount();

        //Then account should appear in the account lists
        accountsSteps.validateAccountHasBeenCreated(accountInfo.get("accountName"));
    }

    @Test(expected = NoSuchElementException.class)
    @Title("When cancelling account setup, account should not be created")
    public void cancelAccountSetup() {
        //Given I have my new account details
        HashMap<String, String> accountInfo = new HashMap<String, String>() {{
            put("accountName","Account should not be created");
            put("accountColor", "Dark Blue");
            put("currency", "UYU - Peso Uruguayo");
            put("accountDesc", "Account should not be created");
        }};

        //When I cancel it
        accountsSteps.clickCreateAccountButton();
        createAccountSteps.createNewAccount(accountInfo);
        createAccountSteps.cancelAccountSetup();

        //Then account should NOT appear in the account lists
        accountsSteps.verifyAccountDoesNotExist(accountInfo.get("accountName"));
    }
}
