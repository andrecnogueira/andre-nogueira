package features.onboarding;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import steps.AccountsSteps;
import steps.OnboardingSteps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SerenityRunner.class)
public class VerifyOnboardingFlow {

    @Managed
    WebDriver webDriver;

    @Steps
    OnboardingSteps onboardingSteps;

    @Steps
    AccountsSteps accountsSteps;

    @Test
    @Title("User should be able to complete onboarding by choosing:" +
            "EUR - Create default accounts - Automatically send crash reports")
    public void completeOnboardingOption1() {
        List<String> onboardingOptions = new ArrayList<>(Arrays.asList(
                "EUR",
                "Create default accounts",
                "Automatically send crash reports"
        ));

        //Given I select my onboarding options
        onboardingSteps.selectOnboardingOptions(onboardingOptions);

        //When I review my changes and confirm
        onboardingSteps.reviewSetupAndConfirm(onboardingOptions);
        accountsSteps.dismissReleaseNotes();

        //Then I should see
        accountsSteps.verifyUserIsOnAccountPage();
    }
}
