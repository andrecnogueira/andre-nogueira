# Home Challenge - André Nogueira

## Task 1 - Testing Monefy app

| Build number | Device Info    | OS Version    |
| :---:        | :---:          | :---:         |
| Build: 1.2.3 | iPhone 8       | iOS 12.1.4    |


#### TC#1 User should be able to install app and launch it
- **Test Steps**
  1. Download the app from store
  2. Open app
- **Expected Results**
  - User should be able to launch the app without any issue.
- **Pass/Fail**
  - Pass

#### TC#2 User should be able to add expense
- **Test Steps**
  1. With app open, click `-` sign
  2. Input amount, ie: 90,00
  3. Tap choose category
  4. Select a given category
- **Expected Results**
  - User should be able to add expense
  - New amount should be added to the `red` count
  - Balance should be updated accordingly (Income - Expense)
- **Pass/Fail**
  - Pass

#### TC#3 User should be able to add income
- **Test Steps**
  1. With app open, click `+` sign
  2. Input amount, ie: 6000
  3. Tap on salary icon, for instance.
- **Expected Results**
  - User should be able to add expense
  - User should see the new income added to the `green` count
  - Balance should be updated accordingly (Income - Expense)
- **Pass/Fail**
  - Pass

#### TC#4 User should be able to delete existing expense
- **Test Steps**
  1. With app open, click Balance label
  2. Select an existing expense and click it
  3. In the top right, click the bin icon
  4. Tap balance, in order to go to main page
- **Expected Results**
  - User should be able to delete given expense
- **Pass/Fail**
  - Pass

#### TC#5 User should be able to delete existing income
- **Test Steps**
  1. With app open, click Balance label
  2. Select an existing income and click it
  3. In the top right, click the bin icon
  4. Tap balance, in order to go to main page
- **Expected Results**
  - User should be able to delete given income
- **Pass/Fail**
  - Pass

#### TC#6 User should be able to edit existing expense
- **Test Steps**
  1. With app open, click Balance label
  2. Select an existing expense and click it
  3. Edit the amount for the Expense and/or expense details
  4. Tap Choose Category button
  5. Select the new category and tap it
- **Expected Results**
  - User should be able to edit existing expense
  - Expense amount and category should be reflected accordingly
- **Pass/Fails**
  - Pass

#### TC#7 User should be able to edit existing income
- **Test Steps**
  1. With app open, click Balance label
  2. Select an existing income and click it
  3. Edit the amount for the income and/or income details
  4. Tap Choose Category button
  5. Select the new category and tap it
- **Expected Results**
  - User should be able to edit existing income
  - Income amount and category should be reflected accordingly
- **Pass/Fails**
  - Pass

#### TC#8 User should be able to update app version without loosing their data
- **Test Steps**
  1. Navigate to the app store
  2. Click `UPDATE` icon for the app
  3. Once the update is complete, launch app again
- **Expected Results**
  - User should be able to launch app
  - User data should be kept during upgrade
- **Pass/Fails**
  - TC not run

#### TC#9 Killing the app will not delete data
- **Test Steps**
  1. Kill the app and launch again
- **Expected Results**
  - All data should be kept
- **Pass/Fails**
  - Pass

#### TC#10 Upgrade to PRO Version should be successful
- **Test Steps**
  1. With the app open, swipe to left in order to open side bar
  2. Tap Settings and then `Unlock Monefy`
  3. Tap `BUY`
- **Expected Results**
  - User should be charged accordingly to advertised price
  - User should unlock all the pro features
- **Pass/Fails**
  - TC not run


## Task 2 - Automated tests for GnuCash Android android

### Tests
I automated few tests for three features I feel it is important to ensure business scenarios are bug free. They are:
 - `Account Flow`: The goal here is verify user can create a custom account
 - `Onboarding`: Make sure the user can start the app for the first time and complete setup
 - `Search`: User should be able to filter accounts while searching


### Framework structure and libraries
Basically I have structured this framework in three layers
 - Tests: Test class containing self explanatory and behavioral tests
 - Test Steps: Basically classes having an user interaction with the app
 - Page Objects: Encapsulation of UI components

The main goal on breaking down in below layers is to improve maintainability and other aspects.

```
+------------+                   +------------+                     +------------+                      +------------+
|            |                   |            |                     |            |                      |            |
|            |                   |            |                     |            |                      |            |
|            |  +--------------> |            |   +-------------->  |            |  +-------------->    |            |
|            |                   |            |                     |            |                      |            |
|    Test    |                   |   Test     |                     |   Page     |                      |     UI     |
|   Classes  |                   |   Steps    |                     |  Objects   |                      |    Layer   |
|            |                   |            |                     |            |                      |            |
|            | <--------------+  |            |   <--------------+  |            |   <--------------+   |            |
|            |                   |            |                     |            |                      |            |
+------------+                   +------------+                     +------------+                      +------------+
```

Libraries used:
 - Serenity, Junit and Appium

### Environment Setup

- **Assumptions**:
 1. Android SDK setup accordingly
 2. Appium installed (v1.10.0)
 3. Java 8
 4. Gradle
 * Ideally your application is running on containers and setup should not be a problem as you can ship it anywhere. However due to time constraints we are doing the old fashioned way :)

- **Running Tests**
 1. Open `serenity.properties` file in the project root dir
    - Set the path to Gnucash app to `appium.app` property
    - Set the emulator name or device name to `appium.deviceName` property
      - `emulator -list-avds` or `adb devices` to get emulator or device name
 2. Start appium
 3. On `automation-sample/mobile` run:
 ```
 $ gradle clean test aggregate
 ```
- **Checking Test Results**
 1. After tests run, Serenity will generate `index.html` file with test results. You can find it in `automation-sample/mobile/target/site/serenity/`


## Task 3 - Testing RESTful API in Best Buy API playground

### Tests
I choose one endpoint, `products`, to perform my tests and interact with the api, which I found really interesting and had good times when playing around with the `api-playground`. The following HTTP methods have been tested:
 - GET: `/products` and `products/id`
 - DELETE: `/products/{id}`
 - PATCH: `/products/{id}`
 - POST: `/products`

### Framework structure and libraries
Basically I have structured this framework in two layers
 - Tests: Test class containing self explanatory and behavioral tests
 - Actions/DTO: These two layers are responsible to communicate with tests as well as restful service

The main goal on breaking down in below layers is to improve maintainability and other aspects.

```
+------------+                   +-------------+                     +------------+
|            |                   |             |                     |            |
|            |                   |             |                     |            |
|            |  +--------------> |  Actions    | +---------------->  |            |
|            |                   |             |                     |            |
| TEST CASES |                   |             |                     |   RESTFUL  |
|            |                   |             |                     |     API    |
|            |                   |Data Transfer|                     |            |
|            |  <--------------+ |   Object    | <----------------+  |            |
|            |                   |             |                     |            |
+------------+                   +-------------+                     +------------+

```
Libraries used:
 - Serenity, Junit, RestAssured, Gson

### Environment Setup
- **Assumptions**:
 1. api-playground` running on your `localhost:3030`
 2. Java 8
 3. Gradle >= 4.10.2
 * Ideally your application is running on containers and setup should not be a problem as you can ship it anywhere. However due to time constraints we are doing the old fashioned way :)

- **Running Tests**
 1. On `automation-sample/api` run:
 ```
 $ gradle clean test aggregate
 ```
- **Checking Test Results**
 1. After tests run, Serenity will generate `index.html` file with test results. You can find it in `automation-sample/api/target/site/serenity/`

## Improvements for the next iteration
 - I'd love to be able to use the swagger file to validate Request/Responses, but apparently the file is broken. I tried to use Attlansian's library [swagger-request-validator](https://bitbucket.org/atlassian/swagger-request-validator/src/master/) however it was not able to parse the swagger file. I noticed `Best Buy` does not update the repo for `api-playground` for the past two years, besides a README.md commit.  I tried with another swagger files and it worked tho.
 - Dockerize my solution!
 - Add a Jenkins file to version control the pipelines
 - Create some scripts, perhaps in Python, to:
   - Setup the environment
     - Spin up emulators, appium and api-playground
  - Triggers tests
  - Teardown the envrionment
    - Kill emulators, api-playground and publish results


# Thank you N26 Team, I had a lot of fun on these three days doing the technical task! Looking forward for the next steps.
