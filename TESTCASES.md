# Task 1

The purpose of the following test plan is to assure all core app functionalities still bug free.


| Build number | Device Info    | OS Version    |
| :---:        | :---:          | :---:         |
| Build: 1.2.3 | iPhone 8       | iOS 12.1.4    |


#### TC#1 User should be able to install app and launch it
- **Test Steps**
  1. Download the app from store
  2. Open app
- **Expected Results**
  - User should be able to launch the app without any issue.
- **Pass/Fail**
  - Pass

#### TC#2 User should be able to add expense
- **Test Steps**
  1. With app open, click `-` sign
  2. Input amount, ie: 90,00
  3. Tap choose category
  4. Select a given category
- **Expected Results**
  - User should be able to add expense
  - New amount should be added to the `red` count
  - Balance should be updated accordingly (Income - Expense)
- **Pass/Fail**
  - Pass

#### TC#3 User should be able to add income
- **Test Steps**
  1. With app open, click `+` sign
  2. Input amount, ie: 6000
  3. Tap on salary icon, for instance.
- **Expected Results**
  - User should be able to add expense
  - User should see the new income added to the `green` count
  - Balance should be updated accordingly (Income - Expense)
- **Pass/Fail**
  - Pass

#### TC#4 User should be able to delete existing expense
- **Test Steps**
  1. With app open, click Balance label
  2. Select an existing expense and click it
  3. In the top right, click the bin icon
  4. Tap balance, in order to go to main page
- **Expected Results**
  - User should be able to delete given expense
- **Pass/Fail**
  - Pass

#### TC#5 User should be able to delete existing income
- **Test Steps**
  1. With app open, click Balance label
  2. Select an existing income and click it
  3. In the top right, click the bin icon
  4. Tap balance, in order to go to main page
- **Expected Results**
  - User should be able to delete given income
- **Pass/Fail**
  - Pass

#### TC#6 User should be able to edit existing expense
- **Test Steps**
  1. With app open, click Balance label
  2. Select an existing expense and click it
  3. Edit the amount for the Expense and/or expense details
  4. Tap Choose Category button
  5. Select the new category and tap it
- **Expected Results**
  - User should be able to edit existing expense
  - Expense amount and category should be reflected accordingly
- **Pass/Fails**
  - Pass

#### TC#7 User should be able to edit existing income
- **Test Steps**
  1. With app open, click Balance label
  2. Select an existing income and click it
  3. Edit the amount for the income and/or income details
  4. Tap Choose Category button
  5. Select the new category and tap it
- **Expected Results**
  - User should be able to edit existing income
  - Income amount and category should be reflected accordingly
- **Pass/Fails**
  - Pass

#### TC#8 User should be able to update app version without loosing their data
- **Test Steps**
  1. Navigate to the app store
  2. Click `UPDATE` icon for the app
  3. Once the update is complete, launch app again
- **Expected Results**
  - User should be able to launch app
  - User data should be kept during upgrade
- **Pass/Fails**
  - TC not run

#### TC#9 Killing the app will not delete data
- **Test Steps**
  1. Kill the app and launch again
- **Expected Results**
  - All data should be kept
- **Pass/Fails**
  - Pass

#### TC#10 Upgrade to PRO Version should be successful
- **Test Steps**
  1. With the app open, swipe to left in order to open side bar
  2. Tap Settings and then `Unlock Monefy`
  3. Tap `BUY`
- **Expected Results**
  - User should be charged accordingly to advertised price
  - User should unlock all the pro features
- **Pass/Fails**
  - TC not run
