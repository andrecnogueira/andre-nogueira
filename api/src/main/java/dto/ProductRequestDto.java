package dto;

public class ProductRequestDto {

    private String name;
    private String type;
    private double price;
    private String upc;
    private Integer shipping;
    private String description;
    private String manufacturer;
    private String model;
    private String url;

    public ProductRequestDto(String name,
                             String type,
                             double price,
                             String upc,
                             Integer shipping,
                             String description,
                             String manufacturer,
                             String model,
                             String url) {
        this.name = name;
        this.type = type;
        this.price = price;
        this.upc = upc;
        this.shipping = shipping;
        this.description = description;
        this.manufacturer = manufacturer;
        this.model = model;
        this.url = url;
    }

    public ProductRequestDto() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public Integer getShipping() {
        return shipping;
    }

    public void setShipping(Integer shipping) {
        this.shipping = shipping;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static ProductRequestDto createProduct(String name,
                                                  String type,
                                                  double price,
                                                  String upc,
                                                  Integer shipping,
                                                  String description,
                                                  String manufacturer,
                                                  String model,
                                                  String url) {
        return new ProductRequestDto(name, type, price, upc, shipping, description, manufacturer, model, url);
    }
}
