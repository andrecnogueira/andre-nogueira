package actions.healthcheck;

import actions.common.CommonActions;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.model.TestStep;

public class HealthCheckActions extends TestStep {

    private final static String HEALTH_CHECK__BASE_URL = "http://127.0.0.1:3030/healthcheck";
    private Response response;

    @Steps
    CommonActions commonActions;

    @Step
    public void doHealthCheck() {
        response = commonActions.performGet(
                HEALTH_CHECK__BASE_URL
        );
    }

    @Step
    public void validateHealthCHeck() {
        response.then().statusCode(200);
    }

    @Step
    public int getNumberOfDocuments(String documentType) {
        doHealthCheck();
        return response.path("documents." + documentType + "");
    }
}
