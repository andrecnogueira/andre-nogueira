package actions.common;

import com.google.gson.Gson;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.model.TestStep;

public class CommonActions extends TestStep {

    private Gson gson = new Gson();

    @Step
    public String serialization(Object objectToSerialize) {
        //serialize json
        return gson.toJson(objectToSerialize);
    }

    @Step
    public Response performPatch(String contentType, Object dtoObject, String baseUrl, int resource) {
        return SerenityRest.
                given().contentType(contentType).
                with().body(serialization(dtoObject)).
                when().patch(baseUrl + "/" + resource);
    }

    @Step
    public Response performGetByResource(String baseUrl, int resourceId) {
        return SerenityRest.
                when().
                get(baseUrl + "/" + resourceId);
    }

    @Step
    public Response performGet(String baseUrl) {
        return SerenityRest.
                when().
                get(baseUrl);
    }

    @Step
    public Response performPost(String contentType, Object dtoObject, String baseUrl) {
        return SerenityRest.
                given().contentType(contentType).
                with().body(serialization(dtoObject)).
                when().post(baseUrl);
    }

    @Step
    public Response performDelete(String baseUrl, int resourceId) {
        return SerenityRest.
                when().
                delete(baseUrl + "/" + resourceId);
    }
}
