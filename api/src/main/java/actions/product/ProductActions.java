package actions.product;

import actions.common.CommonActions;
import actions.healthcheck.HealthCheckActions;
import dto.ProductRequestDto;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.model.TestStep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadLocalRandom;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

public class ProductActions extends TestStep {

    final Logger logger = LoggerFactory.getLogger(ProductActions.class);
    private final static String HEADER_CONTENT_TYPE = "application/json";
    private final static String PRODUCTS_BASE_URL = "http://127.0.0.1:3030/products";
    private int currentNumberOfProducts = 0;
    private Response response;

    @Steps
    HealthCheckActions healthCheckActions;

    @Steps
    CommonActions commonActions;

    @Step
    public void searchProductById(int productId) {
        response = commonActions.performGetByResource(
                PRODUCTS_BASE_URL,
                productId)
        ;
    }

    @Step
    public void validateHttpStatusCode(int expectedStatusCode) {
        response.then().statusCode(expectedStatusCode);
    }

    @Step
    public void productIdShouldMatchSearchedProduct(int productId) {
        response.then()
                .body("id", is(productId));
    }

    @Step
    public void createNewProduct(ProductRequestDto newProduct) {
        currentNumberOfProducts = healthCheckActions.getNumberOfDocuments("products");

        response = commonActions.performPost(
                HEADER_CONTENT_TYPE,
                newProduct,
                PRODUCTS_BASE_URL
        );

        validateHttpStatusCode(201);

        logger.info("New product with id {} created", (int)response.path("id"));
    }

    @Step
    public void validateCountOfProducts(boolean isDelete) {
        int updatedNumberOfExistingProducts = healthCheckActions.getNumberOfDocuments("products");

        if (isDelete) {
            assertTrue((currentNumberOfProducts -=1) == updatedNumberOfExistingProducts);
        } else {
            assertTrue((currentNumberOfProducts +=1) == updatedNumberOfExistingProducts);
        }
    }

    @Step
    public void validateNewProduct() {
        validateCountOfProducts(false);

        int newProductId = response.path("id");
        searchProductById(newProductId);
        productIdShouldMatchSearchedProduct(newProductId);
    }

    public int findRandomProductId() {
        response = commonActions.performGet(
                PRODUCTS_BASE_URL
        );

        int listOfProductsSize = response.path("data.size()");
        int randomIndex = ThreadLocalRandom.current().nextInt(0, listOfProductsSize - 1);

        return response.path("data["+ randomIndex +"].id");
    }

    public void deleteProductById(int productId) {
        response = commonActions.performDelete(
                PRODUCTS_BASE_URL,
                productId
        );

        validateHttpStatusCode(200);
        logger.info("product with ID {} deleted", productId);
    }

    public void validateProductHasBeenDeleted(int productId) {
        searchProductById(productId);

        validateHttpStatusCode(404);
    }

    public void setNewProductPrice(ProductRequestDto newPrice, int productId) {
        response = commonActions.performPatch(
                HEADER_CONTENT_TYPE,
                newPrice,
                PRODUCTS_BASE_URL,
                productId);
    }

    public void validateNewProductPrice(String newPrice) {
        String currentPrice = response.path("price").toString();
        assertTrue(currentPrice.equals(newPrice));
    }
}
