package features.products;

import dto.ProductRequestDto;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Test;
import org.junit.runner.RunWith;
import actions.product.ProductActions;

@RunWith(SerenityRunner.class)
public class ProductTest {

    @Steps
    ProductActions productActions;

    @Test
    @Title("Creating a new product should be successful")
    public void createNewProductShouldBeSuccessful() {
        //Given I have a new product
        ProductRequestDto newProduct = ProductRequestDto.createProduct(
                "iPhone XS Max 512 GB Gold",
                "HardGood",
                1649.00,
                "041333424065",
                0,
                "iPhone XS Max 512 GB Gold 6,5” Display",
                "Apple",
                "MN2400B4Z",
                "https://www.bestbuy.com/site/apple-iphone-xs-max-512gb-gold-at-t/6009670.p?skuId=6009670"
        );

        //When I create the new product
        productActions.createNewProduct(newProduct);

        //Then it should be available
        productActions.validateNewProduct();
        //send the gson to match with
    }

    @Test
    @Title("Deleting a product should be successful")
    public void deleteProduct() {
        //Given I have a product
        int productIdToDelete = productActions.findRandomProductId();

        //When I delete such product
        productActions.deleteProductById(productIdToDelete);

        //Then product should be deleted
        productActions.validateProductHasBeenDeleted(productIdToDelete);
    }

    @Test
    @Title("Search for a valid product")
    public void searchForExistingProductId() {
        //Given I have a product
        int myRandomProduct = productActions.findRandomProductId();

        //When I search for it
        productActions.searchProductById(myRandomProduct);

        //Then I should see it
        productActions.productIdShouldMatchSearchedProduct(myRandomProduct);
    }

    @Test
    @Title("Update price for a current product should be successful")
    public void updateExistingPriceProduct() {
        //Given I have my product
        int productIdToUpdate = productActions.findRandomProductId();
        ProductRequestDto newPrice = new ProductRequestDto();
        newPrice.setPrice(26.26);


        //When I update its price
        productActions.setNewProductPrice(newPrice, productIdToUpdate);

        //Then new price should take place
        productActions.validateNewProductPrice("26.26");
    }

    @Test
    @Title("Search for invalid product should return Status Code 404")
    public void searchForInvalidProduct() {
        //Given I have an invalid product
        int productId = 110000000;

        //When I search for it
        productActions.searchProductById(productId);

        //Then I should see 404
        productActions.validateHttpStatusCode(404);
    }
}
